<?php

session_start();

require '../vendor/autoload.php';

$app = new \Slim\App( array(
    'settings' => array(
        'displayErrorDetails' => true,
    )
));

require __DIR__ . '../app/routes.php';


/*
$container = $app->getContainer();

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};
*/